import { Mesa } from './../models/mesa/mesa.model';
import { MesaProvider } from './../providers/mesa/mesa.service';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, ToastController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { AppMinimize } from '@ionic-native/app-minimize';
import { BackgroundMode } from '@ionic-native/background-mode';

import { LoginPage } from './../pages/login/login';
import { SplashPage } from './../pages/splash/splash';
import { HomePage } from '../pages/home/home';
import { ListaCarrinhoPage } from '../pages/lista-carrinho/lista-carrinho';
import { PedidoControllerPage } from '../pages/pedido-controller/pedido-controller';
import { FavoritoPage } from '../pages/favorito/favorito';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pages: { title: string, icon: string, component: any }[];
  pagesOld: { title: string, icon: string, component: Function }[];
  constructor(
    alertCtrl: AlertController,
    backgroundMode: BackgroundMode,
    mesaPvd: MesaProvider,
    appMinimize: AppMinimize,
    platform: Platform,
    toastCtrl: ToastController,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    storage: Storage
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      statusBar.backgroundColorByHexString('#B2DFDB');
      statusBar.overlaysWebView(true);

      this.pages = [{
        title: 'Home',
        icon: 'home',
        component: HomePage
      }, {
        title: 'Carrinhos',
        icon: 'cart',
        component: ListaCarrinhoPage
      }, {
        title: 'Pedidos',
        icon: 'clipboard',
        component: PedidoControllerPage
      }, {
        title: 'Favoritos',
        icon: 'star',
        component: FavoritoPage
      }];

      this.pagesOld = [{
        title: `Cancelar comanda`,
        icon: 'trash',
        component: () => {
          storage.get('mesa')
            .then((mesa: Mesa) => {
              console.log('Cancelar');
              mesaPvd.available(mesa.id)
                .subscribe((s) => {
                  toastCtrl.create({
                    message: 'Comanda removida',
                    duration: 3000,
                    position: 'bottom',
                    showCloseButton: true,
                    closeButtonText: 'Fechar'
                  }).present();
                  storage.remove('mesa');
                }, (e) => {
                  toastCtrl.create({
                    message: 'Comanda não removida',
                    duration: 3000,
                    position: 'bottom',
                    showCloseButton: true,
                    closeButtonText: 'Fechar'
                  }).present();
                })
            })
        }
      }, {
        title: 'Sair',
        icon: 'exit',
        component: () => {
          alertCtrl.create({
            subTitle: 'Sair?',
            message: 'Tem certeza que deseja sair?',
            buttons: [{
              text: 'Não',
              role: 'cancel'
            }, {
              text: 'Sim',
              handler: () => {
                storage.remove('token').then(() => {
                  window.localStorage.removeItem('token');
                  this.rootPage = LoginPage;
                });
              }
            }]
          })
            .present();
        }
      }];

      storage.ready().then(() => {
        storage.get('token').then((token) => {
          if (token) {
            window.localStorage.setItem('token', token);
            this.rootPage = HomePage;
          } else {
            console.log('Logged?', token);
            this.rootPage = SplashPage;
          }
        }).catch(() => {
          this.rootPage = SplashPage;
        })
      })

      window.setTimeout(() => {
        splashScreen.hide();
      }, 500);

      backgroundMode.enable();
      backgroundMode.setDefaults({ silent: true });
      backgroundMode.configure({
        title: 'Restaurante',
        text: 'Estamos esperado atualizações',
        resume: true,
        hidden: true,
        bigText: false
      });

      backgroundMode
        .on('activate')
        .subscribe(() => {
          backgroundMode.unlock();
          backgroundMode.disableWebViewOptimizations();
        });
      platform.registerBackButtonAction(() => {
        let page = this.nav.getActive().component;
        if (
          page == HomePage ||
          page == LoginPage ||
          page == SplashPage
        ) {
          appMinimize.minimize();
        } else {
          this.nav.getActive().getNav().pop();
        }
      });

    });
  }

  openPage(page: { title: string, icon: string, component: any }) {
    if (this.nav.getActive().component !== page.component) {
      this.nav.push(page.component);
    }
  }

  openPageOld(p: { title: string, icon: string, component: Function }) {
    p.component();
  }
}

