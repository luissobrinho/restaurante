import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { AppMinimize } from '@ionic-native/app-minimize';

import { MyApp } from './app.component';
import { SplashPage } from './../pages/splash/splash';
import { HomePage } from '../pages/home/home';
import { MenuPageModule } from '../pages/menu/menu.module';
import { DetalheProdutoPageModule } from '../pages/detalhe-produto/detalhe-produto.module';
import { CarrinhoPageModule } from '../pages/carrinho/carrinho.module';
import { ListaCarrinhoPageModule } from '../pages/lista-carrinho/lista-carrinho.module';
import { LoginPageModule } from './../pages/login/login.module';
import { CategoriaPageModule } from '../pages/categoria/categoria.module';
import { PedidoControllerPageModule } from '../pages/pedido-controller/pedido-controller.module';
import { FavoritoPageModule } from '../pages/favorito/favorito.module';
import { DetalhePedidoPageModule } from '../pages/detalhe-pedido/detalhe-pedido.module';

import { CarrinhoProvider } from '../providers/carrinho/carrinho.service';
import { LoginProvider } from '../providers/login/login.service';
import { CategoriaProvider } from '../providers/categoria/categoria.service';
import { ComplementoProvider } from '../providers/complemento/complemento.service';
import { FavoritoProvider } from '../providers/favorito/favorito.service';
import { ItemProdutoProvider } from '../providers/item-produto/item-produto.service';
import { MesaProvider } from '../providers/mesa/mesa.service';
import { IonicStorageModule } from '@ionic/storage';
import { PedidoProvider } from '../providers/pedido/pedido.service';
import { ProdutoProvider } from '../providers/produto/produto.service';
import { FechamentoPageModule } from '../pages/fechamento/fechamento.module';
import { BackgroundMode } from '@ionic-native/background-mode';
//import { TaxaProvider } from '../providers/taxa/taxa.service';
//import { UserProvider } from '../providers/user/user.service';
//import { UsuarioProvider } from '../providers/usuario/usuario.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SplashPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__restaurante',
      storeName: 'res',
      driverOrder: ['sqlite', 'indexdb', 'websql', 'localstorage']
    }),
    LoginPageModule,
    MenuPageModule,
    DetalheProdutoPageModule,
    CarrinhoPageModule,
    ListaCarrinhoPageModule,
    CategoriaPageModule,
    PedidoControllerPageModule,
    FavoritoPageModule,
    DetalhePedidoPageModule,
    FechamentoPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SplashPage
  ],
  providers: [
    AppMinimize,
    BackgroundMode,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CarrinhoProvider,
    LoginProvider,
    CategoriaProvider,
    ComplementoProvider,
    FavoritoProvider,
    ItemProdutoProvider,
    MesaProvider,
    PedidoProvider,
    ProdutoProvider,
    //TaxaProvider,
    //UserProvider,
    //UsuarioProvider,
  ]
})
export class AppModule {}
