export interface Produto {
    id: number,
    nome: string,
    descricao: string,
    tipo: number,
    disponivel: true,
    foto: string,
    thumb: string,
    valor: number,
    id_categoria: number,
    created_at: string,
    updated_at: string,
}