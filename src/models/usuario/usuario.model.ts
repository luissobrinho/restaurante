export interface Usuario {
    id: number,
    nome: string,
    data_nascimento: string,
    cargo: string,
    nivel: number,
    id_user: number,
    created_at: string,
    updated_at: string,
}