import { Produto } from './../produto/produto.model';
export interface Carrinho {
    id: number,
    id_produto: number,
    id_user: number,
    id_mesa: number,
    quantidade: number,
    complemento: string[],
    observacao: string,
    valor_adicional: number,
    created_at: string,
    updated_at: string,
    produto: Produto
}