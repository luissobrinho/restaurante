export interface Categoria {
    id: number
    nome: string,
    descricao: string,
    foto: string,
    created_at: string,
    updated_at: string,
}