export interface User {
    id: number,
    CPF: string,
    email: string,
    numero: string,
    usuario: string,
    matricula: string,
    token: string,
    ativo: string,
    created_at: string,
    updated_at: string,
}