import { Produto } from './../produto/produto.model';
export interface Favorito {
    id: number,
    updated_at: string,
    created_at: string,
    produto: Produto
}