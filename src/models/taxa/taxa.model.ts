export interface Taxa {
    id: number,
    valor: number,
    tipo: number,
    descricao: string,
    disponivel: boolean,
    updated_at: string,
    created_at: string,
}