export interface Carrinho {
    id_produto: number,
    id_user: number,
    id_mesa: number,
    quantidade: number,
    observacao: string,
}

export interface Categoria {
    nome: string,
    descricao: string,
    foto: string,
}

export interface Complemento {
    id_produto: number,
    id_complemento: number,
}

export interface Favorito {
    id_produto: number
}

export interface Mesa {
	numero: number,
    quantidade: number,
    disponivel: boolean,
    local: string,
}

export interface Produto {
    nome : string,
    descricao : string,
    tipo : number,
    foto?: string,
    thumb?: string,
    disponivel : boolean,
    valor: number,
    id_categoria : number,
}

export interface Taxa {
    valor: number,
    tipo: number,
    descricao: string,
    disponivel: boolean,
}

export interface User {
    CPF: string,
    email: string,
    numero: string,
    usuario: string,
    matricula: string,
    token: string,
    ativo: string,
    password: string,
}

export interface Usuario {
    nome: string,
    data_nascimento: string,
    cargo: string,
    nivel: number,
    id_user: number,
}

export interface Pedido {
    id_user: number,
    id_mesa: number,
    forma_pagamento: number,
    pago: number,
    valor: number
}