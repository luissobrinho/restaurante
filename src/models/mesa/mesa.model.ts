export interface Mesa {
    id: number
    numero: string,
    quantidade: number,
    disponivel: boolean,
    local: string,
    created_at: string,
    updated_at: string,
}