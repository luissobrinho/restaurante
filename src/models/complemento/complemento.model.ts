import { Produto } from './../produto/produto.model';
export interface Complemento {
    id: number,
    id_produto: number,
    id_complemento: number,
    created_at: string,
    updated_at: string,
    complemento: Produto
}