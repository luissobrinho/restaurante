import { Produto } from './../produto/produto.model';
export interface Produtos {
    current_page: number,
    data: Produto[],
    first_page_url: string,
    from: number,
    last_page: number,
    last_page_url: string,
    next_page_url: string,
    path: string,
    per_page: number,
    prev_page_url: string,
    to: number,
    total: number,
}