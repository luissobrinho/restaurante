export { Usuario } from './usuario/usuario.model';
export { User } from './user/user.model';
export { Taxa } from './taxa/taxa.model';
export { Produto } from './produto/produto.model';
export { Produtos } from './produtos/produtos.model';
export { Pedido } from './pedido/pedido.model';
export { Pedidos } from './pedidos/pedidos.model';
export { Mesa } from './mesa/mesa.model';
export { ItemProduto } from './item-produto/item-produto.model';
export { Favorito } from './favorito/favorito.model';
export { Favoritos } from './favoritos/favoritos.model';
export { Complemento } from './complemento/complemento.model';
export { Categoria } from './categoria/categoria.model';
export { Carrinho } from './carrinho/carrinho.model';
export interface Message {
    message: string
}