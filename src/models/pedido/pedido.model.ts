export interface Pedido {
    id: number,
    id_user: number,
    id_mesa: number,
    forma_pagamento: number,
    pago: number,
    valor: number,
    created_at: string,
    updated_at: string,
}