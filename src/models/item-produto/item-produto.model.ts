import { Produto } from "../request.module";
import { Taxa } from "../taxa/taxa.model";

export interface ItemProduto {
    produtos: {
        id: number,
        id_pedido: number,
        id_produto: number,
        observacao: string,
        quantidade: number,
        tipo: string,
        novo: boolean,
        created_at: string,
        updated_at: string,
        produto: Produto
    }[],
    taxas: {
        id: number,
        id_pedido: number,
        id_produto: number,
        observacao: string,
        quantidade: number,
        tipo: string,
        novo: boolean,
        created_at: string,
        updated_at: string,
        taxa: Taxa
    }[]
}