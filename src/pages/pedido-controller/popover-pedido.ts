import { FechamentoPage } from './../fechamento/fechamento';
import { MesaProvider } from './../../providers/mesa/mesa.service';
import { CarrinhoPage } from './../carrinho/carrinho';
import { DetalhePedidoPage } from './../detalhe-pedido/detalhe-pedido';
import { Component } from "@angular/core";
import { ViewController, NavParams, ModalController, App } from "ionic-angular";
import * as Response from '../../models/response.module';

@Component({
    template: `
    <ion-list no-margin>
    <ion-item (click)="showPedidos(1)">Aberto</ion-item>
    <ion-item (click)="showPedidos(2)">Comcluidos</ion-item>
    <ion-item (click)="showPedidos(3)">Fechado</ion-item>
    <ion-item (click)="showPedidos(0)">Todos</ion-item>
  </ion-list>
    `
})
export class PopoverPedido {
    constructor(public viewCtrl: ViewController, public navParams: NavParams) {

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    showPedidos(status: number) {
        console.log('status', status);
        this.viewCtrl.dismiss(status);
    }
}

@Component({
    template: `
    <ion-list no-margin>
    <ion-item (click)="openDetalhe()">Detalhe</ion-item>
  </ion-list>
    `
})
export class PopoverOpcionalUm {

    pedido: Response.Pedido;

    constructor(
        public viewCtrl: ViewController,
        public modalCtrl: ModalController,
        public navParams: NavParams
    ) {

        this.pedido = this.navParams.get('pedido')

    }

    openDetalhe() {
        this.viewCtrl.dismiss().then(() => {
            let modal = this.modalCtrl.create(DetalhePedidoPage, { acao: 'Detalhe', pedido: this.pedido })

            modal.present();
        });
    }
}

@Component({
    template: `
    <ion-list no-margin>
    <ion-item (click)="open()">Abrir</ion-item>
    <ion-item (click)="openDetalhe()">Detalhe</ion-item>
  </ion-list>
    `
})
export class PopoverOpcionalDois {

    pedido: Response.Pedido;
    mesa: Response.Mesa;
    constructor(
        public app: App,
        public viewCtrl: ViewController,
        public mesaPvd: MesaProvider,
        public modalCtrl: ModalController,
        public navParams: NavParams
    ) {

        this.pedido = this.navParams.get('pedido')
        this.mesaPvd.selectMesa()
            .then((mesa: Response.Mesa) => {
                this.mesa = mesa;
            });
    }

    open() {
        this.viewCtrl.dismiss().then(() => {
            this.app.getActiveNav().push(CarrinhoPage, { mesa: this.mesa });
        });
    }

    openDetalhe() {
        this.viewCtrl.dismiss().then(() => {
            let modal = this.modalCtrl.create(DetalhePedidoPage, { acao: 'Detalhe', pedido: this.pedido })

            modal.present();
        });
    }
}

@Component({
    template: `
    <ion-list no-margin>
    <ion-item (click)="open()">Abrir</ion-item>
    <ion-item (click)="close()">Fechar</ion-item>
    <ion-item (click)="openDetalhe()">Detalhe</ion-item>
  </ion-list>
    `
})
export class PopoverOpcionalTres {

    pedido: Response.Pedido;
    mesa: Response.Mesa;
    constructor(
        public app: App,
        public viewCtrl: ViewController,
        public mesaPvd: MesaProvider,
        public modalCtrl: ModalController,
        public navParams: NavParams
    ) {

        this.pedido = this.navParams.get('pedido')
        this.mesaPvd.selectMesa()
            .then((mesa: Response.Mesa) => {
                this.mesa = mesa;
            });
    }

    open() {
        this.viewCtrl.dismiss().then(() => {
            this.app.getActiveNav().push(CarrinhoPage, { mesa: this.mesa });
        });
    }

    close() {
        this.viewCtrl.dismiss().then(() => {
            this.modalCtrl.create(FechamentoPage, { pedido: this.pedido }).present();
        });
    }

    openDetalhe() {
        this.viewCtrl.dismiss().then(() => {
            let modal = this.modalCtrl.create(DetalhePedidoPage, { acao: 'Detalhe', pedido: this.pedido })

            modal.present();
        });
    }
}