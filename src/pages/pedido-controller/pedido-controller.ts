import { MesaProvider } from './../../providers/mesa/mesa.service';
import { PedidoProvider } from './../../providers/pedido/pedido.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Popover, ToastController } from 'ionic-angular';
import { PopoverPedido, PopoverOpcionalUm, PopoverOpcionalDois, PopoverOpcionalTres } from './popover-pedido';

import * as Response from '../../models/response.module';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';

@IonicPage()
@Component({
  selector: 'page-pedido-controller',
  templateUrl: 'pedido-controller.html',
})
export class PedidoControllerPage {
  items: number[][];
  status: number = 0;
  itemsShow: number[];
  pedidos: Response.Pedido[];
  constructor(
    public mesaPvd: MesaProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public pedidoPvd: PedidoProvider,
    public toastCtrl: ToastController,
  ) {
    this.tipoStatus();
    this.loadPedidos();
  }

  loadPedidos() {
    this.pedidoPvd.statusOnView(this.status)
      .map((res) => {
        res.forEach((r) => {
          this.getNumeroMesa(r.id_mesa)
            .then((numero) => {
              r['mesa'] = numero;
            }).catch((e) => {
              r['mesa'] = e;
            });
        });
        return res;
      })
      .subscribe((pedidos: Response.Pedido[]) => {
        this.pedidos = pedidos;
        console.log(pedidos);

      }, (e) => {
        let data = e.json();

        if (e.status == 404) {
          this.toastCtrl.create({
            message: data.message,
            showCloseButton: true,
            closeButtonText: 'Fechar',
            position: 'bottom'
          }).present();
        }
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidoControllerPage');
  }

  more(event) {
    let popover: Popover = this.popoverCtrl.create(PopoverPedido);

    popover.present({
      ev: event
    });

    popover.onDidDismiss((status: number) => {
      if (status >= 0 && status != null) {
        console.log('Show Pedidos: ', status);
        this.status = status;
        this.loadPedidos();
      }
    })
  }

  itemSelected(event, item) {

    switch (this.status) {
      case 0:
        console.log('Detalhe');
        this.popoverCtrl.create(PopoverOpcionalUm, { pedido: item })
          .present({ ev: event });
        break;
      case 1:
        console.log('Reativar ou detalhes');
        this.popoverCtrl.create(PopoverOpcionalDois, { pedido: item })
          .present({ ev: event });
        break;
      case 2:
        console.log('Reativar, pagar ou detalhes');
        this.popoverCtrl.create(PopoverOpcionalTres, { pedido: item })
          .present({ ev: event });
        break;
      case 3:
        console.log('Detalhe');
        this.popoverCtrl.create(PopoverOpcionalUm, { pedido: item })
          .present({ ev: event });
        break;
      default:
        break;
    }
    this.tipoStatus();
  }

  tipoStatus() {
    switch (this.status) {
      case 0:
        return 'Todos'
      case 1:
        return 'Abertos'
      case 2:
        return 'Concluidos'
      case 3:
        return 'Fechado'
    }
  }

  getNumeroMesa(id_mesa: number): Promise<string> {
    var numero: string = '';
    this.mesaPvd
      .show(id_mesa)
      .first()
      .subscribe((mesa: Response.Mesa) => {
        numero = mesa.numero;
      });
    return new Promise<string>((resolver, reject) => {
      (numero !== "") ? resolver(numero) : reject('?');
    })
  }

}
