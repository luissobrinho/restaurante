import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoControllerPage } from './pedido-controller';
import { PopoverPedido, PopoverOpcionalUm, PopoverOpcionalDois, PopoverOpcionalTres } from './popover-pedido';

@NgModule({
  declarations: [
    PedidoControllerPage,
    PopoverPedido,
    PopoverOpcionalUm,
    PopoverOpcionalDois,
    PopoverOpcionalTres,
  ],
  imports: [
    IonicPageModule.forChild(PedidoControllerPage),
  ],
  entryComponents: [
    PopoverPedido,
    PopoverOpcionalUm,
    PopoverOpcionalDois,
    PopoverOpcionalTres,
  ]
})
export class PedidoControllerPageModule { }
