import { Observable } from 'rxjs/Observable';
import { ProdutoProvider } from './../../providers/produto/produto.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { DetalheProdutoPage } from '../detalhe-produto/detalhe-produto';

import * as Response from '../../models/response.module';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  categoria: Response.Categoria;
  produtos: Observable<Response.Produtos>;
  mesa: Response.Mesa;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public produtoPvd: ProdutoProvider
  ) {
    this.categoria = this.navParams.get('categoria');
    this.produtos = this.produtoPvd
      .menu(this.categoria.id);
    this.mesa = this.navParams.get('mesa');
  }

  ionViewDidLoad() {
  }

  openProduto(produto: Response.Produto) {
    this.modalCtrl.create(DetalheProdutoPage, {
      produto: produto,
      title: this.categoria.nome,
      mesa: this.mesa
    }).present();
  }

}
