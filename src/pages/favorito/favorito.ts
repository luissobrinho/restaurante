import { Observable } from 'rxjs/Observable';
import { FavoritoProvider } from './../../providers/favorito/favorito.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController } from 'ionic-angular';
import { DetalheProdutoPage } from '../detalhe-produto/detalhe-produto';
import * as Response from '../../models/response.module';

@IonicPage()
@Component({
  selector: 'page-favorito',
  templateUrl: 'favorito.html',
})
export class FavoritoPage {
  favoritos: Observable<Response.Favoritos>;
  constructor(
    public alertCtrl: AlertController,
    public favoritoPvd: FavoritoProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController
  ) {
    this.loadFavoritos();
  }

  loadFavoritos() {
    this.favoritos = this.favoritoPvd.index();

    this.favoritos
      .subscribe((favoritos: Response.Favoritos) => { }, (e) => {
        let data = e.json();
        this.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 5000
        }).present();
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritoPage');
  }

  openProduto(nome: String) {
    this.modalCtrl.create(DetalheProdutoPage, { nome: nome, title: 'Favorito' })
      .present();
  }

  remove(favorito: Response.Favorito) {
    console.log(favorito);
    
    this.alertCtrl.create({
      subTitle: 'Deseja remover?',
      message: 'Tem certeza que deseja remover o produto dos favoritos?',
      buttons: [{
        text: 'Sim',
        handler: () => {
          this.favoritoPvd.destroy(favorito.id)
            .subscribe((favorito: Response.Favorito) => {
              this.toastCtrl.create({
                message: 'Removido!',
                position: 'bottom',
                duration: 3000
              }).present();
              this.loadFavoritos();
            }, (e) => {
              let data = e.json();
              this.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 5000
              }).present();
            });
        }
      }, {
        text: 'Não',
        role: 'cancel'
      }]
    }).present();
  }

}
