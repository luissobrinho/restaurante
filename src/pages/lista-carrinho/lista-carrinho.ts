import { CarrinhoProvider } from './../../providers/carrinho/carrinho.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CarrinhoPage } from '../carrinho/carrinho';
import * as Response from '../../models/response.module';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-lista-carrinho',
  templateUrl: 'lista-carrinho.html',
})
export class ListaCarrinhoPage {
  mesas: Observable<Response.Mesa[]>;
  constructor(
    public carrinhoPvd: CarrinhoProvider,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.mesas = this.carrinhoPvd.index();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaCarrinhoPage');
  }

  itemSelected(mesa: Response.Mesa) {
    this.navCtrl.push(CarrinhoPage, { mesa: mesa })
  }

}
