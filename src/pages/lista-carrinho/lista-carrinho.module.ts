import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaCarrinhoPage } from './lista-carrinho';

@NgModule({
  declarations: [
    ListaCarrinhoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaCarrinhoPage),
  ],
})
export class ListaCarrinhoPageModule {}
