import { LoginProvider } from './../../providers/login/login.service';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  fromLogin: FormGroup;

  constructor(
    public alertCtrl: AlertController,
    public app: App,
    formBuild: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    statusBar: StatusBar,
    public loginPvd: LoginProvider,
    public storage: Storage,
  ) {

    statusBar.backgroundColorByHexString('#B2DFDB');
    this.fromLogin = formBuild.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  submitForm() {
    console.log(this.fromLogin.value);
    this.loginPvd
      .authenticate(this.fromLogin.value)
      .subscribe((s) => {
        this.storage.set('token', s.token)
          .then(() => {
            window.localStorage.setItem('token', s.token);
            this.loginPvd.getToken();
            this.app.getActiveNav().setRoot(HomePage);
          });
      }, (e) => {
        let data = e.json();
        if (e.status === 401) {
          this.alertCtrl.create({
            subTitle: 'Falha no login',
            message: data.message
          }).present();
        }
      });
  }

}
