import { DetalheProdutoPage } from './../detalhe-produto/detalhe-produto';
import { Pedido } from './../../models/request.module';
import { PedidoProvider } from './../../providers/pedido/pedido.service';
import { CarrinhoProvider } from './../../providers/carrinho/carrinho.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController, Loading, PopoverController, ViewController } from 'ionic-angular';
import { CategoriaPage } from '../categoria/categoria';
import { Observable } from 'rxjs/Observable';
import * as Response from '../../models/response.module';


@IonicPage()
@Component({
  selector: 'page-carrinho',
  templateUrl: 'carrinho.html',
})
export class CarrinhoPage {
  disabledButton: boolean = true;

  mesa: Response.Mesa;
  total: number = 0;
  carrinhos: Observable<Response.Carrinho[]>;
  pedido: Pedido;
  constructor(
    public alertCtrl: AlertController,
    public carrinhoPvd: CarrinhoProvider,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public pedidoPvd: PedidoProvider,
    public popoverCtrl: PopoverController
  ) {
    if (this.navParams.get('mesa'))
      this.mesa = this.navParams.get('mesa');
    this.pedido = {
      id_user: 1,
      id_mesa: this.mesa.id,
      valor: 0,
      pago: 0,
      forma_pagamento: 0
    }
    this.loadCarrinho();
  }

  loadCarrinho() {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    loading.present();
    this.total = 0;
    this.carrinhos = this.carrinhoPvd.show(this.mesa.id);
    this.carrinhos.subscribe((carrinhos) => {
      carrinhos.forEach((carrinho) => {
        this.total += ((carrinho.produto.valor + carrinho.valor_adicional) * carrinho.quantidade);
      });
      this.disabledButton = false;
      loading.dismiss();
    }, (e) => {

      let data = e.json();

      if (e.status === 404) {
        this.alertCtrl.create({
          subTitle: data.message,
          buttons: ['OK']
        }).present();
        this.disabledButton = true;
      }
      loading.dismiss();
    })
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad CarrinhoPage');
  }

  openMenu() {
    this.navCtrl.push(CategoriaPage, { mesa: this.mesa });
  }

  more(event, c: Response.Carrinho) {
    let popover = this.popoverCtrl.create(PopoverCarrinho, {
      carrinho: c,
      mesa: this.mesa
    });

    popover.present({
      ev: event
    });

    popover.onDidDismiss((data, role) => {
      if (role == 'edit') this.loadCarrinho();
    });

  }

  sendPedido() {
    if (this.mesa.id !== 0) {
      this.alertCtrl.create({
        subTitle: 'Enviar pedido?',
        message: `O pedido da mesa ${this.mesa.numero}`,
        buttons: [{
          text: 'Cancelar'
        }, {
          text: 'Enviar',
          handler: () => {
            this.pedidoPvd.store(this.pedido)
              .subscribe((s) => {
                this.alertCtrl.create({
                  subTitle: 'Pedido efetuado com sucesso',
                  buttons: [{
                    text: 'OK',
                    handler: () => {
                      this.navCtrl.popToRoot();
                    }
                  }]
                }).present();
              }, (e) => {
                let data = e.json();
                console.log(data);
                this.alertCtrl.create({
                  subTitle: data.message,
                  buttons: ['OK']
                }).present();
              });
          }
        }]
      }).present();
    } else {
      this.alertCtrl.create({
        subTitle: 'Selecione a mesa!',
        buttons: ['OK']
      }).present();
    }
  }

}

@Component({
  template: `
  <ion-list no-margin>
  <ion-item (click)="edit()">Editar</ion-item>
  <ion-item (click)="remover()">Remover</ion-item>
  </ion-list>
  `
})

export class PopoverCarrinho {
  carrinho: Response.Carrinho;
  mesa: Response.Mesa;
  constructor(
    public alertCtrl: AlertController,
    public carrinhoPvd: CarrinhoProvider,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
  ) {
    this.mesa = this.navParams.get('mesa');
    this.carrinho = this.navParams.get('carrinho');
  }

  edit() {
    let modal = this.modalCtrl.create(DetalheProdutoPage, {
      carrinho: this.carrinho,
      title: 'Carrinho',
      mesa: this.mesa
    });

    modal.present();

    modal.onDidDismiss((data: Response.Produto, role) => {
      this.viewCtrl.dismiss(data, role);
    });
  }

  remover() {
    console.log("Remover Produto do carrinho!");
    this.carrinhoPvd.destroy(this.carrinho.id).subscribe((carrinho: Response.Carrinho) => {
      this.viewCtrl.dismiss(null, 'edit');
    }, (e) => {
      let data = e.json();
      console.log(data);
      this.alertCtrl.create({
        subTitle: data.message,
        buttons: ['OK']
      }).present();
    });
  }
}