import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarrinhoPage, PopoverCarrinho } from './carrinho';

@NgModule({
  declarations: [
    CarrinhoPage,
    PopoverCarrinho,
  ],
  imports: [
    IonicPageModule.forChild(CarrinhoPage),
  ],
  entryComponents: [
    PopoverCarrinho,
  ]
})
export class CarrinhoPageModule { }
