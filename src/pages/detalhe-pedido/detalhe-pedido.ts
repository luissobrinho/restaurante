import { ModalController } from 'ionic-angular';
import { ItemProdutoProvider } from './../../providers/item-produto/item-produto.service';
import { Observable } from 'rxjs/Observable';
import { PedidoProvider } from './../../providers/pedido/pedido.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, App } from 'ionic-angular';
import * as Response from '../../models/response.module';
import { MesaProvider } from '../../providers/mesa/mesa.service';
import { CarrinhoPage } from '../carrinho/carrinho';
import { FechamentoPage } from '../fechamento/fechamento';

/**
 * Generated class for the DetalhePedidoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhe-pedido',
  templateUrl: 'detalhe-pedido.html',
})
export class DetalhePedidoPage {
  acao: string = 'Detalhe';
  pedido: Response.Pedido;
  pedidoTotal: any;
  items: Observable<Response.ItemProduto>;
  mesa: Response.Mesa;
  constructor(
    public app: App,
    public alertCtrl: AlertController,
    public itemProduto: ItemProdutoProvider,
    mesaPvd: MesaProvider,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public pedidoPvd: PedidoProvider,
    public viewCtrl: ViewController,
  ) {
    this.acao = this.navParams.get('acao');
    this.pedido = this.navParams.get('pedido');

    console.log(this.pedido);
    this.pedidoPvd.show(this.pedido.id).subscribe((data) => {
      this.pedidoTotal = data[0];
      console.log(this.pedidoTotal);
    });

    this.items = this.itemProduto.show(this.pedido.id);

    mesaPvd
      .show(this.pedido.id_mesa)
      .subscribe((mesa: Response.Mesa) => {
        this.mesa = mesa;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalhePedidoPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  mover() {
    this.pedidoPvd.movePedido(this.pedido.id, 2)
      .subscribe(() => {
        this.alertCtrl.create({
          subTitle: 'Movido!',
          buttons: [{
            text: 'OK',
            handler: () => {
              this.viewCtrl.dismiss();
            }
          }]
        }).present();
      }, (e) => {
        let data = e.json();
        this.alertCtrl.create({
          subTitle: data.message,
          buttons: ['OK']
        }).present();
      });
  }

  open() {
    this.viewCtrl.dismiss().then(() => {
      this.app
        .getActiveNav()
        .push(CarrinhoPage, { mesa: this.mesa });
    })
  }

  pagar() {
    this.viewCtrl.dismiss().then(() => {
      this.modalCtrl.create(FechamentoPage, { pedido: this.pedido }).present();
    });
  }

}