import { PedidoProvider } from './../../providers/pedido/pedido.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as Response from '../../models/response.module';

@IonicPage()
@Component({
  selector: 'page-fechamento',
  templateUrl: 'fechamento.html',
})
export class FechamentoPage {
  formFechamento: FormGroup;
  pedido: Response.Pedido;
  constructor(
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public pedidoPvd: PedidoProvider,
    public viewCtrl: ViewController,
  ) {
    this.pedido = this.navParams.get('pedido');

    this.formFechamento = this.formBuilder.group({
      forma_pagamento: ["1", [Validators.required]],
      pago: ["0.00", [Validators.required, Validators.min(0.01)]],
      status: [3]
    });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FechamentoPage');
  }

  dinheiro() {

    this.formFechamento.controls['pago'].setValue(this.formFechamento.controls['pago'].value.replace(/^(0+)/g, ""));
    this.formFechamento.controls['pago'].setValue(this.formFechamento.controls['pago'].value.replace(/\./g, ""));
    let len = this.formFechamento.controls['pago'].value.length;
    if (1 == len)
      this.formFechamento.controls['pago'].setValue(this.formFechamento.controls['pago'].value.replace(/(\d)/, "0.0$1"));
    else if (2 == len)
      this.formFechamento.controls['pago'].setValue(this.formFechamento.controls['pago'].value.replace(/(\d)/, "0.$1"));
    else if (len > 2) {
      this.formFechamento.controls['pago'].setValue(this.formFechamento.controls['pago'].value.replace(/(\d{2})$/, '.$1'));
    }
    console.log(this.formFechamento.controls['pago'].value);
  }

  formSend() {
    console.log(this.formFechamento.value);
    this.pedidoPvd.pagar(this.pedido.id, this.formFechamento.value)
      .subscribe((pedido: Response.Pedido) => {
        this.alertCtrl.create({
          subTitle: 'Pagamento registrado',
          message: 'O pagamento foi registrado e a mesa foi liberada!',
          buttons: [{
            text: 'OK',
            handler: () => {
              this.dismiss();
            }
          }]
        }).present();
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}