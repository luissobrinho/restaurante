import { CategoriaProvider } from './../../providers/categoria/categoria.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import * as Response from '../../models/response.module';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the CategoriaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categoria',
  templateUrl: 'categoria.html',
})
export class CategoriaPage {
  categorias: Observable<Response.Categoria[]>;
  showCategoria: boolean = false;
  mesa: Response.Mesa;
  constructor(
    public categoriaPvd: CategoriaProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController
  ) {
    this.categorias = this.categoriaPvd.index();
    this.mesa = this.navParams.get('mesa');
  }

  ionViewDidEnter() {
    this.categorias
      .subscribe((c) => {
        if (c.length) {
          this.showCategoria = true;
        }
      }, (e) => {
        if (e.status == 404) {
          this.toastCtrl.create({
            message: 'Não há categorias cadastradas',
            showCloseButton: true,
            closeButtonText: 'Fechar',
            position: 'bottom'
          }).present();
        }
      })
  }

  openMenu(categoria: Response.Categoria) {
    this.navCtrl.push(MenuPage, { categoria: categoria, mesa: this.mesa });
  }

}