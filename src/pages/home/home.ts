import { CarrinhoPage } from './../carrinho/carrinho';
import { CategoriaPage } from './../categoria/categoria';
import { MesaProvider } from './../../providers/mesa/mesa.service';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import * as Response from '../../models/response.module';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  mesas: Observable<Response.Mesa[]>;
  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    statusBar: StatusBar,
    public mesaPvd: MesaProvider
  ) {
    statusBar.backgroundColorByHexString('#00796B');
    this.loadMesas();
  }

  loadMesas() {
    this.mesas = this.mesaPvd.index();
    this.mesas.subscribe(() => { }, (e) => {
      if (e.status == 404 || e.status == 401) {
        setTimeout(() => {
          this.loadMesas();
          this.mesaPvd.getToken();
        }, 500)
      }
    })
  }

  openMenu(mesa: Response.Mesa) {
    if (mesa.disponivel) {
      this.alertCtrl.create({
        subTitle: 'Abrir comanda?',
        message: `Deseja abrir uma comanda para mesa <b>${mesa.numero}</b>?`,
        buttons: [{
          text: 'Sim',
          handler: () => {
            this.mesaPvd.available(mesa.id)
              .subscribe(() => {
                this.mesaPvd.insertMesa(mesa);
                this.navCtrl.push(CategoriaPage, { title: 'Mesa ' + mesa.numero, mesa: mesa });
                this.loadMesas();
              });
          }
        }, {
          text: 'Não'
        }]
      }).present();
    } else {
      this.alertCtrl.create({
        title: 'Ocupada',
        message: 'Esta mesa está ocupada!<br />Ir ao carrinho?',
        buttons: [{
          text: 'Sim',
          handler: () => {
            this.mesaPvd.insertMesa(mesa);
            this.navCtrl.push(CarrinhoPage, { mesa: mesa });
          }
        }, {
          text: 'Não'
        }]
      }).present();
    }

  }

}
