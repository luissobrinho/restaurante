import { FavoritoProvider } from './../../providers/favorito/favorito.service';
import { CarrinhoProvider } from './../../providers/carrinho/carrinho.service';
import { MesaProvider } from './../../providers/mesa/mesa.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ComplementoProvider } from './../../providers/complemento/complemento.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, AlertButton, App, ToastController } from 'ionic-angular';
import { CarrinhoPage } from '../carrinho/carrinho';
import * as Response from '../../models/response.module';

@IonicPage()
@Component({
  selector: 'page-detalhe-produto',
  templateUrl: 'detalhe-produto.html',
})
export class DetalheProdutoPage {
  selectOptions: { title: string; subTitle: string; };
  produto: Response.Produto;
  mesa: Response.Mesa;
  title: String = '';
  complementos: Response.Complemento[];
  formCarrinho: FormGroup;
  carrinho: Response.Carrinho;
  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public carrinhoPvd: CarrinhoProvider,
    public favoritoPvd: FavoritoProvider,
    formBuild: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public mesaPvd: MesaProvider,
    public complementoPvd: ComplementoProvider,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
  ) {
    if (this.navParams.get('carrinho')) {
      this.carrinho = this.navParams.get('carrinho');
      this.title = this.navParams.get('title');
      this.mesa = this.navParams.get('mesa');
      this.produto = this.carrinho.produto;
      console.log(this.carrinho);

      this.complementoPvd
        .show(this.produto.id)
        .subscribe((complementos: Response.Complemento[]) => {
          this.complementos = complementos;
        });

      let obs = '';
      if (this.carrinho.observacao) {
        obs = this.carrinho.observacao.split(' Complementos')[0]
      }

      this.formCarrinho = formBuild.group({
        id: [this.carrinho.id],
        id_produto: [this.produto.id, [Validators.required]],
        id_user: [1, [Validators.required]],
        id_mesa: [this.mesa.id, [Validators.required]],
        quantidade: [this.carrinho.quantidade, [Validators.required]],
        observacao: [obs],
        complemento: [this.carrinho.complemento]
      })
    } else {
      this.produto = this.navParams.get('produto');
      this.title = this.navParams.get('title');
      this.mesa = this.navParams.get('mesa');

      this.complementoPvd
        .show(this.produto.id)
        .subscribe((complementos: Response.Complemento[]) => {
          this.complementos = complementos;
        });
      this.formCarrinho = formBuild.group({
        id_produto: [this.produto.id, [Validators.required]],
        id_user: [1, [Validators.required]],
        id_mesa: [this.mesa.id, [Validators.required]],
        quantidade: [1, [Validators.required]],
        observacao: [""],
        complemento: [[]]
      })
    }

    this.selectOptions = {
      title: 'Complementos',
      subTitle: 'Adicione os complementos'
    };



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalheProdutoPage');
  }

  dismiss() {
    this.viewCtrl.dismiss(this.produto, 'none');
  }

  onSubmit() {
    (this.title == 'Carrinho') ? this.updateCarrinho() : this.addCarrinho();
  }

  isTitle(): string {
    return (this.title == 'Carrinho') ? 'Alterar' : 'Adicionar';
  }

  private updateCarrinho() {
    console.log(this.formCarrinho.value);
    this.carrinhoPvd.update(this.formCarrinho.value)
      .subscribe((carrinho: Response.Carrinho) => {
        this.showAlert('Alterado com sucesso!', [
          {
            text: 'OK',
            handler: () => {
              this.viewCtrl.dismiss(this.produto, 'edit');
            }
          }
        ]);
      }, (e) => {
        let data = e.json();
        this.alertCtrl.create({
          subTitle: data.message,
          buttons: ['OK']
        })
      });
  }

  private addCarrinho() {
    console.log(this.formCarrinho.value);
    this.carrinhoPvd.store(this.formCarrinho.value)
      .subscribe((carrinho: Response.Carrinho) => {
        this.showAlert('Deseja ir ao carrinho?',
          [{
            text: 'Sim',
            handler: () => {
              this.viewCtrl.dismiss(this.produto, 'create').then(() => {
                this.app.getActiveNav().push(CarrinhoPage, { mesa: this.mesa })
              });
            }
          }, {
            text: 'Não',
            handler: () => {
              this.viewCtrl.dismiss(null, 'none');
            }
          }]);
      }, (e) => {
        let data = e.json();
        this.alertCtrl.create({
          subTitle: data.message,
          buttons: ['OK']
        })
      })
  }

  private showAlert(message: string, buttons: AlertButton[]) {
    this.alertCtrl.create({
      subTitle: message,
      buttons: buttons
    }).present()
  }

  addFavorito() {
    this.favoritoPvd
      .store({ id_produto: this.produto.id })
      .subscribe(() => {
        this.toastCtrl.create({
          message: 'Produto adicionado!',
          position: 'bottom',
          duration: 3000
        }).present();
      })
  }

}