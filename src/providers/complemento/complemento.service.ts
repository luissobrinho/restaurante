import { MethodsAPI } from './../method.inf';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

/*
  Generated class for the ComplementoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ComplementoProvider extends BaseService implements MethodsAPI {

  constructor(protected http: Http) {
    super(http);
  }

  index(): Observable<Response.Complemento[]> {
    return this.get(`/complemento`)
  }

  show(id_produto: number): Observable<Response.Complemento[]> {
    return this.get(`/complemento/${id_produto}`)
  }

  store(complemento: Request.Complemento): Observable<Response.Complemento> {
    return this.post(`/complemento`, complemento);
  }

  update(complemento: Response.Complemento): Observable<Response.Complemento> {
    return this.put(`/complemento/${complemento.id}`, complemento);
  }

  destroy(id: number): Observable<Response.Complemento> {
    return this.delete(`/complemento/${id}`)
  }

}
