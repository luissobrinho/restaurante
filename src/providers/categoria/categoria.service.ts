import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BaseService } from '../base/base.service';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

/*
  Generated class for the CategoriaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoriaProvider extends BaseService {

  constructor(protected http: Http) {
    super(http);
  }

  index(): Observable<Response.Categoria[]> {
    return this.get(`/categoria`);
  }

  show(id): Observable<Response.Categoria> {
    return this.get(`/categoria/${id}`);
  }

  store(categoria: Request.Categoria): Observable<Response.Categoria> {
    return this.post(`/categoria`, categoria);
  }

  update(categoria: Response.Categoria): Observable<Response.Categoria> {
    return this.put(`/categoria/${categoria.id}`, categoria);
  }

  destroy(id): Observable<Response.Categoria> {
    return this.delete(`/categoria/${id}`);
  }

}
