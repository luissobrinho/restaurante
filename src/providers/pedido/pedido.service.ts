import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import { MethodsAPI } from './../method.inf';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

@Injectable()
export class PedidoProvider extends BaseService implements MethodsAPI {

  constructor(protected http: Http) {
    super(http);
  }

  index(): Observable<Response.Pedidos> {
    return this.get(`/pedido`);
  }

  show(id: number): Observable<Response.Pedido> {
    return this.get(`/pedido/${id}`);
  }

  store(pedido: Request.Pedido): Observable<Response.Pedido> {
    return this.post(`/pedido`, pedido);
  }

  update(pedido: Response.Pedido): Observable<Response.Pedido> {
    return this.put(`/pedido/${pedido.id}`, pedido);
  }

  destroy(id: number): Observable<Response.Pedido> {
    return this.get(`/pedido/${id}`);
  }

  statusOnView(status: number): Observable<Response.Pedido[]> {
    return this.get(`/pedido/status/${status}`);
  }

  movePedido(id_pedido: number, status: number): Observable<Response.Pedido> {
    return this.get(`/pedido/${id_pedido}/move/${status}`);
  }

  pagar(id_pedido: number, data: any): Observable<Response.Pedido> {
    return this.post(`/pedido/${id_pedido}/pagar`, data);
  }

}
