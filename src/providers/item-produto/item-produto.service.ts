import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import { MethodsAPI } from './../method.inf';
import * as Response from '../../models/response.module';

@Injectable()
export class ItemProdutoProvider extends BaseService implements MethodsAPI {

  constructor(public http: Http) {
    super(http);
  }

  index(): Observable<any> {
    throw new Error("Method not implemented.");
  }

  show(id_pedido: number): Observable<Response.ItemProduto> {
    return this.get(`/item-pedido/${id_pedido}`);
  }

  store(data: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  update(data: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  destroy(id: number): Observable<any> {
    throw new Error("Method not implemented.");
  }

}
