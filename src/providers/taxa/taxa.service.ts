import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import { MethodsAPI } from './../method.inf';
import * as Response from '../../models/response.module';

/*
  Generated class for the TaxaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaxaProvider extends BaseService implements MethodsAPI {

  constructor(protected http: Http) {
    super(http);
  }

  index(): Observable<Response.Taxa[]> {
    return this.get(`/taxa`);
  }

  show(id: number): Observable<Response.Taxa> {
   return this.get(`/taxa/${id}`);
  }

  store(taxa: Response.Taxa): Observable<Response.Taxa> {
   return this.post(`/taxa`, taxa);
  }

  update(taxa: Response.Taxa): Observable<Response.Taxa> {
   return this.put(`/taxa/${taxa.id}`, taxa);
  }

  destroy(id: number): Observable<Response.Taxa> {
   return this.get(`/taxa/${id}`);
  }

  available(id: number) : Observable<Response.Taxa> {
    return this.get(`/taxa/${id}/available`);
  }

}
