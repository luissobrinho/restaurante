import { MethodsAPI } from './../method.inf';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

@Injectable()
export class FavoritoProvider extends BaseService implements MethodsAPI {

  constructor(protected http: Http) {
    super(http);
  }

  index(): Observable<Response.Favoritos> {
    return this.get('/favorito')
  }
  
  show(id: number): Observable<Response.Favorito> {
    throw new Error("Method not implemented.");
  }

  store(favorito: Request.Favorito): Observable<Response.Favorito> {
    return this.post(`/favorito`, favorito);
  }
  
  update(favorito: Response.Favorito): Observable<Response.Favorito> {
    throw new Error("Method not implemented.");
  }
  
  destroy(id: number): Observable<Response.Favorito> {
    return this.delete(`/favorito/${id}`);
  }

}
