import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import { MethodsAPI } from './../method.inf';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider extends BaseService implements MethodsAPI {

  constructor(public http: Http) {
    super(http);
  }

  index(): Observable<Response.User> {
    return this.get(`/user`);
  }

  show(id: number): Observable<Response.User> {
    return this.get(`/user/${id}`);
  }

  store(user: Request.User): Observable<Response.User> {
    return this.post(`/user`, user);
  }

  update(user: Response.User): Observable<Response.User> {
    return this.put(`/user/${user.id}`, user);
  }

  destroy(id: number): Observable<Response.User> {
    return this.delete(`/user/${id}`);
  }

  refresh() {
    return this.get(`/authenticate/refresh`)
  }

  getInfo(): Observable<Response.User> {
    return this.get(`/user/get-info`);
  }

}
