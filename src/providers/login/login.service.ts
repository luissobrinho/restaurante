import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { BaseService } from '../base/base.service';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider extends BaseService {

  constructor(protected http: Http) {
    super(http);
  }

  authenticate(dadosLogin: { email: string, password: string }): Observable<{token: string}> {
    let headers: Headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(`http://restaurante.huntersolucoes.com.br/api/authenticate/hMuOMqr9OM4obFZdKircuxjQvtzURWcR`,
      dadosLogin, {
        headers: headers
      }).map((res) => res.json());
  }

}
