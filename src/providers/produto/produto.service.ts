import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import { MethodsAPI } from './../method.inf';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';
/*
  Generated class for the ProdutoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProdutoProvider extends BaseService implements MethodsAPI {

  constructor(protected http: Http) {
    super(http)
  }

  index(): Observable<Response.Produtos> {
    return this.get(`/produto`);
  }

  show(id: number): Observable<Response.Produto> {
    return this.get(`/produto/${id}`);
  }

  store(produto: Request.Produto): Observable<Response.Produto> {
    return this.post(`/produto`, produto);
  }

  update(produto: Response.Produto): Observable<Response.Produto> {
    return this.put(`/produto/${produto.id}`, produto);
  }

  destroy(id: number): Observable<Response.Produto> {
    return this.delete(`/produto/${id}`);
  }

  available(id): Observable<Response.Produto> {
    return this.get(`/produto/${id}/available`);
  }

  menu(id_categoria): Observable<Response.Produtos> {
    return this.get(`/produto/${id_categoria}/menu`);
  }

}
