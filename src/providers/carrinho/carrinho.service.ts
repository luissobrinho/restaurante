import { MethodsAPI } from './../method.inf';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BaseService } from '../base/base.service';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

/*
  Generated class for the CarrinhoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CarrinhoProvider extends BaseService implements MethodsAPI {

  constructor(protected http: Http) {
    super(http);
  }

  index(): Observable<Response.Mesa[]> {
    return this.get(`/carrinho`);
  }

  show(id_mesa: number): Observable<Response.Carrinho[]> {
    return this.get(`/carrinho/${id_mesa}`);
  }

  store(carrinho: Request.Carrinho): Observable<Response.Carrinho> {
    return this.post(`/carrinho`, carrinho);
  }

  update(carrinho: Response.Carrinho): Observable<Response.Carrinho> {
    return this.put(`/carrinho/${carrinho.id}`, carrinho);
  }

  destroy(id: number): Observable<Response.Carrinho> {
    return this.delete(`/carrinho/${id}`);
  }

}
