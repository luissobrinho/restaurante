import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import { MethodsAPI } from './../method.inf';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider extends BaseService implements MethodsAPI {
  
  constructor(protected http: Http) {
    super(http);
  }

  index(): Observable<Response.Usuario> {
    return this.get(`/usuario`);
  }

  show(id: number): Observable<Response.Usuario> {
    return this.get(`/usuario/${id}`);
  }

  store(usuario: Request.Usuario): Observable<Response.Usuario> {
    return this.post(`/usuario`, usuario);
  }

  update(usuario: Response.Usuario): Observable<Response.Usuario> {
    return this.put(`/usuario/${usuario.id}`, usuario);
  }

  destroy(id: number): Observable<Response.Usuario> {
    return this.delete(`/usuario/${id}`);
  }

  

}
