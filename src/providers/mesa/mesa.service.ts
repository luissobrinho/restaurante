import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../base/base.service';
import { MethodsAPI } from './../method.inf';
import * as Response from '../../models/response.module';
import * as Request from '../../models/request.module';

@Injectable()
export class MesaProvider extends BaseService implements MethodsAPI {

  constructor(protected http: Http, public storage: Storage) {
    super(http);
  }

  index(): Observable<Response.Mesa[]> {
    return this.get(`/mesa`);
  }

  show(id: number): Observable<Response.Mesa> {
    return this.get(`/mesa/${id}`);
  }

  store(mesa: Request.Mesa): Observable<Response.Mesa> {
    return this.post(`/mesa`, mesa);
  }

  update(mesa: Response.Mesa): Observable<Response.Mesa> {
    return this.put(`/mesa`, mesa);
  }

  destroy(id: number): Observable<Response.Mesa> {
    return this.delete(`/mesa/${id}`);
  }

  available(id: number): Observable<Response.Mesa> {
    return this.get(`/mesa/${id}/available`);
  }

  insertMesa(mesa: Response.Mesa): Promise<any> {
    return this.storage.set('mesa', mesa);
  }

  selectMesa(): Promise<Response.Mesa> {
    return this.storage.get('mesa');
  }

}
