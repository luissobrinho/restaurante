import { Observable } from 'rxjs/Observable';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

export abstract class BaseService {
  private options: RequestOptions;
  private header: Headers;
  private token: string = '?token=';
  private url_server: string = 'http://restaurante.huntersolucoes.com.br/api'

  constructor(protected http: Http) {

    this.header = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.header });
    this.token += window.localStorage.getItem('token');
  }

  getToken() {
    this.token += window.localStorage.getItem('token');
  }

  protected get(controller: string): Observable<any> {
    return this.http.get(`${this.url_server}${controller}${this.token}`)
      .map((res) => res.json());
  }

  protected post(controller: string, data: any): Observable<any> {
    return this.http.post(`${this.url_server}${controller}${this.token}`, data, this.options)
      .map((res) => res.json());
  }

  protected put(controller: string, data: any): Observable<any> {
    return this.http.put(`${this.url_server}${controller}${this.token}`, data, this.options)
      .map((res) => res.json());
  }

  protected delete(controller: string): Observable<any> {
    return this.http.delete(`${this.url_server}${controller}${this.token}`, this.options)
      .map((res) => res.json());
  }

  public url(url: string): Observable<any> {
    return this.get(`${url}${this.token}`).map((res) => res.json());
  }

}
